import {Hero} from './hero';


export const HEROES: Hero[] =[
    {id: 11, name: 'John'},
    {id: 12, name: 'Joe'},
    {id: 13, name: 'Dan'},
    {id: 14, name: 'Tammy'},
    {id: 15, name: 'Joshua'},
    {id: 16, name: 'Susan'},
    {id: 17, name: 'Kim'},
    {id: 18, name: 'Chi'},
    {id: 11, name: 'Lee'},
    {id: 20, name: 'Wayne'},
];