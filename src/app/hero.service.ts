import { Injectable } from '@angular/core';
import { Hero } from './hero';
import { HEROES } from './mock-heroes';
import {Observable, of} from 'rxjs';
import {MessageService} from './message.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, map, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HeroService {

  private herosUrl = 'api/heroes';

  httpOptions =  {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private http: HttpClient,
    private messageService: MessageService){}
  
  private log(message: string){
    this.messageService.add(`HeroSerive: ${message}`);
  }


  getHeroes(): Observable<Hero[]>{
    this.messageService.add('HeroService: fetched heroes');
    return this.http.get<Hero[]>(this.herosUrl).pipe(tap(_ => this.log('fetched heroes')),
                                                     catchError(this.handleError<Hero[]>('getHeroes', [])));
  }

  getHero(id : number): Observable<Hero>{
    const url = `${this.herosUrl}/${id}`;
    return this.http.get<Hero>(url).pipe(
      tap(_ => this.log(`fetched hero id = ${id}`)),
      catchError(this.handleError<Hero>(`getHero id= ${id}`))
    );
  }

  //Tap into the Observable
  //The HeroService methods will tap into the flow of observable values and send a message, via a log() method,
  // to the message area at the bottom of the page.
 
  //tap() operator: looks at the observable values, does something with those values, and passes them along.
  // The tap() call back doesn't touch the values themselves.

  //getHero(id: number): Observable<Hero>{
  //  this.messageService.add(`HeroService: fetched hero id: ${id}`);
  //  return of(HEROES.find(hero => hero.id === id));
  //}

  private handleError<T>(operation = 'operation', result?: T){
    return (error: any): Observable<T> => {
      console.error(error);
      this.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    }
  }

  updateHero(hero: Hero): Observable<any>{


    return this.http.put(this.herosUrl, hero, this.httpOptions).pipe(tap(_=>this.log(`updated hero id= ${hero.id}`)), catchError(this.handleError<any>('updateHero')));
  }

  addHero(hero: Hero): Observable<Hero>{
    return this.http.post<Hero>(this.herosUrl, hero, this.httpOptions).pipe(
      tap((newHero: Hero) => this.log(`add hero w/ id = ${newHero.id}`)),
      catchError(this.handleError<Hero>(`addHero`))
    );
  }

  deleteHero(hero: Hero | number): Observable<Hero> {
    const id = typeof hero === 'number' ? hero : hero.id;
    const url = `${this.herosUrl}/${id}`;
  
    return this.http.delete<Hero>(url, this.httpOptions).pipe(
      tap(_ => this.log(`deleted hero id=${id}`)),
      catchError(this.handleError<Hero>('deleteHero'))
    );
  }

  searchHeroes(term: string): Observable<Hero[]>{
    if(!term.trim()){
      //if not search term, return empty hero array
      return of([]);
    }

    return this.http.get<Hero[]>(`${this.herosUrl}/?name=${term}`).pipe(
      tap(x => x.length ?
        this.log(`found heroes matching "${term}"`):
        this.log(`no heroes match "${term}"`),

        catchError(this.handleError<Hero[]>('serachHeroes', []))
      )
    );
  }


}
