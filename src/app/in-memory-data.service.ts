import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Hero } from './hero';

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService {

  constructor() { }
  createDb(){
    const heroes = [
      {id: 11, name: 'John'},
      {id: 12, name: 'Dan'},
      {id: 13, name: 'Narco'},
      {id: 14, name: 'Celeritas'},
      {id: 15, name: 'Magneta'},
      {id: 16, name: 'Akakatsuki'},
      {id: 17, name: 'Phương Mai'},
      {id: 18, name: 'Thảo Anh'},
      {id: 19, name: 'Bình Bò'},
      {id: 20, name: 'Long Le'},
    ];

    return {heroes};
  }

  genId(heroes: Hero[]): number{
    return heroes.length > 0 ? Math.max(...heroes.map(hero => hero.id)) + 1 : 11;
  }
}
